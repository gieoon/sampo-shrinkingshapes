package com.example.jun.sampo;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by gieoon on 19/08/2017.
 */

public class BadTriangle extends Shape {

    public BadTriangle(Context context) {
        super(context);
        this.shapeType = SHAPE.TRIANGLE;
        this.b_amIBad = true;
        //this.x -= this.scaled_width / 2;
    }

    @Override
    protected void instantiateBitMap() {
        switch(this.type){
            case GREEN : {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bad_triangle_g);
                break;
            }
            case BLUE : {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bad_triangle_b);
                break;
            }
            case YELLOW: {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bad_triangle_y);
                break;
            }
            case RED : {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bad_triangle_r);
                break;
            }
        }
        getResizedBitmap(this.original_bitmap, this.original_bitmap.getWidth() / (int)sizeDeduction, this.original_bitmap.getHeight() / (int)sizeDeduction);
        //this.sizeManager.setSize(this.bitmap.getWidth());
    }

    @Override
    public void update(float delta){
        super.update(delta);
        this.badFlash();
    }

    @Override
    public void draw(Canvas canvas, Paint paint, float delta) {
        super.draw(canvas, paint, delta);
    }
}

package com.example.jun.sampo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.view.GestureDetectorCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.gms.appinvite.AppInviteInvitation;

/**
 * Created by Jun on 3/10/2017.
 * The menu state
 */

public class MenuState {

    private Context context;
    private DisplayMetrics dm;
    private String version_text = "SAMPO v0.0.1 made by juuno (21/08/2017)";
    private Bitmap logo_text = BitmapFactory.decodeResource(context.getResources(), R.drawable.sampo_logo2);
    //private ImageButton shareButton;

    public MenuState(Context context, DisplayMetrics displayMetrics){
        this.context = context;
        this.dm = displayMetrics;
//        this.shareButton.setMinimumWidth(100);
//        this.shareButton.setMinimumHeight(100);
//        this.shareButton.setOnTouchListener(new View.OnTouchListener(){
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                Log.w("TOUCH", "Touch Event Received");
//
//            }
//        })
//        this.shareButton.setBackground(R.mipmap.share_icon);


    }

    public void update(float d){
        //trigger fadeout if button is clicked.
    }

    public void draw(Canvas canvas, Paint paint){
        //draw the buttons
        paint.setColor(Color.BLACK);
        paint.setTextSize(12);
        canvas.drawText(this.version_text, this.dm.widthPixels / 5 , 5 , paint);

        canvas.drawBitmap(
                this.logo_text,
                this.dm.widthPixels / 2 - this.logo_text.getWidth() / 2,
                this.dm.heightPixels / 2 - this.logo_text.getHeight() / 2,
                paint);
    }



}

package com.example.jun.sampo;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by Jun on 5/09/2017.
 * Plays all the musics
 */

public class MediaManager {

    private Context context;
    public static int loaded_id, loss_id, shape_break_id, goal_id;
    public static MediaPlayer loss_sound, goal_sound, loaded_sound;
    public static MediaPlayer[] goal_sounds = new MediaPlayer[3];
    public static MediaPlayer[] loaded_sounds = new MediaPlayer[3];
    public MediaManager(Context context) {
        this.context = context;
        this.loaded_id = context.getResources().getIdentifier("loaded", "raw", context.getPackageName());
        this.loss_id = context.getResources().getIdentifier("loss", "raw", context.getPackageName());
        //this.shape_break_id = context.getResources().getIdentifier("shape_break", "raw", context.getPackageName());
        this.goal_id = context.getResources().getIdentifier("goal", "raw", context.getPackageName());

        for(int i = 0; i < 3; i ++){
             goal_sounds[i] = MediaPlayer.create(this.context, MediaManager.goal_id);
            loaded_sounds[i] = MediaPlayer.create(this.context, MediaManager.loaded_id);
        }
        loss_sound = MediaPlayer.create(this.context, MediaManager.loss_id);
    }

    static void playFirstAvailableSound(MediaPlayer[] mp){
        for(int i = 0; i < mp.length; i++){
            if(!mp[i].isPlaying()){
                mp[i].start();
                break;
            }
        }
    }
}

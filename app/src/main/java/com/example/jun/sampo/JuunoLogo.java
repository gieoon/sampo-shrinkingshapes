package com.example.jun.sampo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Jun on 14/11/2017.
 */

public class JuunoLogo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //dont' evens et up view, it's from styles.xml
        //a layout file is loaded too late, so this will laod immediately
        Intent intent = new Intent(this, SAMPO_MAIN.class);
        startActivity(intent);
        finish();
    }
}

package com.example.jun.sampo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.os.SystemClock;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Jun on 22/08/2017.
 * static class to create generic delays
 */

public class Delay extends MenuObject{

    private int delayAmount;
    private long startTime;
    private SAMPO_MAIN.GAMESTATE finalState;
    private Handler handler;
    private Runnable runnable;

    public Delay(int delayAmount, SAMPO_MAIN.GAMESTATE gamestate,Context context){
        super();
        final Context c = context;
        this.finalState = gamestate;
        this.delayAmount = delayAmount;
        this.startTime = SystemClock.elapsedRealtime();

//        this.handler = new Handler();
//        this.runnable = new Runnable(){
//            @Override
//            public void run(){
//                SAMPO_MAIN.gamestate = finalState;
//            }
//        };
//        this.handler.postDel
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                SAMPO_MAIN.gamestate = finalState;
                //choose the activity to show
                //changing to show highscore in menu, so just two screens
                Intent in = new Intent(c, SAMPO_TITLE.class);//SAMPO_HIGHSCORE.class);
                in.putExtra("score", SAMPO_MAIN.score);
                c.startActivity(in);
            }
        }, 3000);
    }

    @Override
    public void setLocation() {
        //NO ACTION
    }

    @Override
    public void update(){
        long difference = SystemClock.elapsedRealtime() - this.startTime;
        if (difference >= this.delayAmount * 100000){
            //SAMPO_MAIN.gamestate = finalState;
        }
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        //NO ACTION
    }

}

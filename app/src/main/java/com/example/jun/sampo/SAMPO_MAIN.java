package com.example.jun.sampo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by Jun on 1/08/2017.
 * Activity where the main page of the game runs
 * Should this be made in Xamarin & C#, or in Cordova? Wow.
 */

public class SAMPO_MAIN extends AppCompatActivity {

    private SAMPO_LOOP sampo_loop;
    public static MediaManager mediaManager;
    public static int WIDTH = 0;
    public static float speed_increment = 0.0f;
    public static float SPEED_INCREMENT_AMOUNT = 0.02f;
    public static float STARTING_SPEED, TERMINAL_SPEED;
    public static int HEIGHT = 0;
    public static boolean b_gameover;
    public static int score;
    public static int combo = 1;//calculates the current combo, keeps building up as a multiplier!!!

    private ImageButton shareButton;// = (ImageButton) R.id.
    public enum GAMESTATE {
        PLAYING, MENU, HIGHSCORE
    }

    public static GAMESTATE gamestate = GAMESTATE.MENU;

    //private AdView mAdView;//for banner ads...using layout
    private InterstitialAd mInterstitialAd;

    private void restart(){
        SAMPO_MAIN.speed_increment = 0.0f;
        SAMPO_MAIN.b_gameover = false;
        SAMPO_MAIN.combo = 1;
        SAMPO_MAIN.score = 0;
        //these are automatically set to 0 as they are killed when the process / activity is killed.
        if(ShapeHandler.shapeHandler != null)
            ShapeHandler.shapeHandler.emptyList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);


        this.mediaManager = new MediaManager(this);
        BitMapTypes.context = this;
        //mAdView = (AdView) findViewById(R.id.adView);
        //AdRequest adRequest = new AdRequest.Builder().build();
        //mAdView.loadAd(adRequest);
        restart();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        WIDTH = displayMetrics.widthPixels;
        HEIGHT = displayMetrics.heightPixels;
        TileGrid.instantiateGameBoundaries(displayMetrics);
        ShapeHandler.initShapeGenerator(this);

        //Initializing game view object
        sampo_loop = new SAMPO_LOOP(this, displayMetrics);

    //    setContentView(sampo_loop);
        //WARNING changing the context view from a traditional surface view, to a shader GLSurfaceView version!!!
        //if(this.sampo_loop.supportsES2) {
            //Toast.makeText(this, "Using GLSurfaceView Layout", Toast.LENGTH_SHORT).show();
            //setContentView(this.sampo_loop);
          //  setContentView(this.sampo_loop.getGLSurfaceView());
            //this.sampo_loop
            //SurfaceView sv = (SurfaceView) findViewById(this.sampo_loop);
        //}
        //else{
            //Toast.makeText(this, "Using Alternative Layout", Toast.LENGTH_SHORT).show();
            setContentView(this.sampo_loop);
        //}
        //add the menu layout over the top
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        View menuView = LayoutInflater.from(this).inflate(R.layout.activity_sampo__title, null, false);
        addContentView(menuView, lp);
        shareButton = (ImageButton) findViewById(R.id.shareButton);
        shareButton.setOnTouchListener(new View.OnTouchListener() {
                                           @Override
                                           public boolean onTouch(View v, MotionEvent event) {
                                               onInviteClicked();
                                               return false;
                                           }
                                       }
        );

        mInterstitialAd = new InterstitialAd(this);
        //using test unit ID for development. default interstitial test unit ID
        //mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.setAdUnitId("ca-app-pub-9859357986849249/4014890262");
        //.setAdUnitId("ca-app-pub-123456789/123456789");
        AdRequest adRequest = new AdRequest.Builder()
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                //.addTestDevice("abc")
                .addTestDevice("F83EE0B1ABFD8EA7816F835FB6418694")
                .build();
        //mInterstitialAd.loadAd(new AdRequest.Builder().build());

        //check if we are using a test configured device

        Log.w("IS TEST DEVICE?", " " + adRequest.isTestDevice(this));

        //a (i) button, which will show the credits of Juuno.
        //everything is linked from this menu page.
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded(){
                displayInterstitial();
            }
        });
    }

    public void displayInterstitial(){
        //If ads are loaded, show interstitial, else, show nothing
        if (mInterstitialAd.isLoaded()){
            mInterstitialAd.show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w("", "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                for (String id : ids) {
                    Log.w("", "onActivityResult: sent invitation " + id);
                }
            } else {
                // Sending failed or it was canceled, show failure message to the user
                // ...
            }
        }
    }

    public static final int REQUEST_INVITE = 99;
    //callback after the invite button has been clicked.
    private void onInviteClicked() {
        Intent intent = new AppInviteInvitation.IntentBuilder("Want to get your friend to try out SAMPO")
                .setMessage("Try to score as many points as you can in this challenging game!!")
                //TODO set a deep link for the user
                //.setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
                //.setCustomImage(Uri.parse(getString(R.drawable.sampo_logo2))
                // .setCallToActionText(getString(R.string.invitation_cta))
                .build();
        startActivityForResult(intent, REQUEST_INVITE);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unbindDrawables(this.sampo_loop);
        System.gc();//Here comes Mr. Garbage Collector
    }

    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.sampo_loop.getDetector().onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    protected void onPause(){
        super.onPause();
        sampo_loop.pause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        sampo_loop.resume();
    }

}

package com.example.jun.sampo;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Jun on 19/08/2017.
 * Where the CIA resides
 * in every one of these babies.
 */

public class Pentagon extends Shape {

    public Pentagon(Context context) {
        super(context);
        this.shapeType = SHAPE.PENTAGON;
        //this.x -= this.scaled_width / 2;
    }

    @Override
    protected void instantiateBitMap() {
        switch(this.type){
            case GREEN : {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.pentagon_g);
                break;
            }
            case BLUE : {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.pentagon_b);
                break;
            }
            case YELLOW: {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.pentagon_y);
                break;
            }
            case RED : {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.pentagon_r);
                break;
            }
        }
        getResizedBitmap(this.original_bitmap, this.original_bitmap.getWidth() / (int)sizeDeduction, this.original_bitmap.getHeight() / (int)sizeDeduction);
        //this.sizeManager.setSize(this.bitmap.getWidth());
    }

    @Override
    public void update(float delta){
        super.update(delta);
    }

    @Override
    public void draw(Canvas canvas, Paint paint, float delta) {
        super.draw(canvas, paint, delta);
    }
}

/* Renderscript file to create falling snow effect */
/*
#pragma version(1)
#pragma rs java_package_name(com.example.jun.sampo)
#include "rs_graphics.rsh"

rs_mesh snowMesh;

typedef struct__attribute__((packed, aligned(4))) Snow {
    float2 velocity;
    float2 position;
    uchar4 color;
} Snow_t;

Snow_t *snow;

float2 wind;
float2 grav;


int root(){
    //entry point of this script
    //the return value defined how often this script will run, 0 is once, or else is maximum that the device is capable of.
    rsgClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    rsgDrawMesh(snowMesh);
    return 0;
}

int init(){
    //called once when the script loads and can be used ot init variables
    grav.x = 0;
    grav.y = 18;
    wind.x = rsRand(50) + 20;
    wind.y = rsRand(4) - 2;
}

void initSnow(){
    const float w = rsgGetWidth();
    const float h = rsgGetHeight();

    int snowCount = rsAllocationGetDimX(rsGetAllocation(snow));

    Snow t *pSnow = snow;
    for(int i = 0; i < snowCount; i++){
        pSnow -> position.x = rsRand(w);
        pSnow -> position.y = rsRand(h);

        pSnow -> velocity.x = rsRand(60);
        pSnow -> velocity.y = rsRand(100);
        pSnow -> velocity.x -= 50;

        uchar4 c = rsPackColorTo8888(255, 255, 255);
        pSnow -> color = x;
        //this line pSnow++ stumps me...what???
        pSnow++;
    }



}

package com.example.jun.sampo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Random;

import static android.content.ContentValues.TAG;

/**
 * Created by Jun on 3/09/2017.
 * Checks if the shape is the same as current filter, if it is, then move it.
 */

public class SideShapesManager extends MenuObject {
    private Bitmap leftShape, rightShape, currentShape;
    private float shapeXLeft, middleX, middleY;
    private float shapeXRight;
    private float shapeY;
    private Context context;
    public static Shape.SHAPE currentShapeType;
    private final int SIZE_DEDUCTION_MINI = 6;
    //private final int SIZE_DEDUCTION_MIDDLE = 0.5;

    public SideShapesManager(Context context, DisplayMetrics displayMetrics){
        Bitmap dummyShape = BitmapFactory.decodeResource(context.getResources(), R.drawable.triangle_back);
        this.middleX = displayMetrics.widthPixels / 2 - dummyShape.getWidth() / 4;
        this.middleY = displayMetrics.heightPixels / 2 - dummyShape.getHeight() / 3;
        dummyShape = this.getResizedBitmap(dummyShape, dummyShape.getWidth() / SIZE_DEDUCTION_MINI, dummyShape.getHeight() / SIZE_DEDUCTION_MINI);
        this.shapeY = displayMetrics.heightPixels / 2 - dummyShape.getHeight() / 2;
        this.shapeXLeft = TileGrid.MARGIN * 2;
        this.shapeXRight = displayMetrics.widthPixels - dummyShape.getWidth() - TileGrid.MARGIN * 2;
        this.context = context;

        //means that no shapes tick.
        currentShapeType = Shape.shapeTypes.get(new Random().nextInt(Shape.SHAPE_TYPE_SIZE));//Shape.SHAPE.NONE;
    }

    @Override
    public void setLocation() {}

    @Override
    public void update() {
        setNeighbouringShapes();
    }

    private void setNeighbouringShapes(){
        int index = Shape.SHAPE.valueOf(this.currentShapeType.name()).ordinal();
        int left_index = index - 1;
        if(left_index < 0){
            left_index = Shape.SHAPE_TYPE_SIZE - 1;
        }
        this.rightShape = getCurrentShape(Shape.shapeTypes.get(left_index), SIZE_DEDUCTION_MINI);

        int right_index = index + 1;
        if(right_index == Shape.SHAPE_TYPE_SIZE){
            right_index = 0;
        }
        this.leftShape = getCurrentShape(Shape.shapeTypes.get(right_index), SIZE_DEDUCTION_MINI);

        currentShape = getCurrentShape(currentShapeType, 2);
    }

        public final Bitmap getCurrentShape(Shape.SHAPE shapeType, int sizeDeduction){
        //declare that a shape has been assigned.
        Bitmap b;
        switch(shapeType){
            case TRIANGLE : {
                b = BitmapFactory.decodeResource(context.getResources(), R.drawable.triangle_back);
                break;
            }
            case CIRCLE : {
                b = BitmapFactory.decodeResource(context.getResources(), R.drawable.circle_back);
                break;
            }
            case SQUARE : {
                b = BitmapFactory.decodeResource(context.getResources(), R.drawable.square_back);
                break;
            }
            case PENTAGON: {
                b = BitmapFactory.decodeResource(context.getResources(), R.drawable.pentagon_back);
                break;
            }
//            case NONE : {
//                //make a dummy resource to draw
//                //b =  BitmapFactory.decodeResource(context.getResources(), R.drawable.square_back);
//                //b =  getResizedBitmap(b, b.getWidth() / (sizeDeduction * 50), b.getHeight() / (sizeDeduction * 50));
//                b = null;
//                break;
//            }
            default : {
                Log.w(TAG, "Error loading shape background image");
                return null;
            }
        }
        //if(shapeType != Shape.SHAPE.NONE){
        //if(b != null) {
            return getResizedBitmap(b, b.getWidth() / sizeDeduction, b.getHeight() / sizeDeduction);
        //}
        //return null;
    }



    @Override
    public void draw(Canvas canvas, Paint paint) {
        //draw another big one of the current shape in the middle!!!
        if(this.leftShape != null)
            canvas.drawBitmap(this.leftShape, this.shapeXLeft, this.shapeY, paint);
        if(this.rightShape != null)
            canvas.drawBitmap(this.rightShape, this.shapeXRight, this.shapeY, paint);
        //if(currentShapeType != Shape.SHAPE.NONE){
            //currentShape.recycle();
            //if(!currentShape.isRecycled()) {

        if(currentShape != null)
            canvas.drawBitmap(currentShape, middleX, middleY, paint);
            //}
        //}
    }

    public final Shape.SHAPE getCurrentShape(){
        return this.currentShapeType;
    }
    public void setShapeType(Shape.SHAPE shape){
        this.currentShapeType = shape;
    }
}

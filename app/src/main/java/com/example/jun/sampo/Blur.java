package com.example.jun.sampo;

import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

/**
 * Created by Jun on 24/09/2017.
 */

public class Blur {

    private static final float BLUR_RADIUS = 10.0f;
    private Context context;

    public Blur(Context context){
        this.context = context;
    }

    public Bitmap blurImage(Bitmap image){
        if(image == null){
            return null;
        }
        Bitmap newBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(this.context);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, newBitmap);

        //Intrinsic Gaussian Blur Filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(newBitmap);
        return newBitmap;
    }
}

package com.example.jun.sampo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by Jun on 3/10/2017.
 */

public class BitMapTypes {

    public static Context context;
    private static Bitmap
            green_square, blue_square, yellow_square, red_square,
            green_circle, blue_circle, yellow_cirlce, red_circle,
            green_triangle, blue_triangle, yellow_triangle, red_triangle,
            green_pentagon, blue_pentagon, yellow_pentagon, red_pentagon;

    public void initBitMaps(){
        green_square = BitmapFactory.decodeResource(context.getResources(), R.drawable.square_g);
        blue_square = BitmapFactory.decodeResource(context.getResources(), R.drawable.square_b);
        yellow_square = BitmapFactory.decodeResource(context.getResources(), R.drawable.square_y);
        red_square = BitmapFactory.decodeResource(context.getResources(), R.drawable.square_r);
    }

}

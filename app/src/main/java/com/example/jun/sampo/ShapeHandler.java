package com.example.jun.sampo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/**
 * Created by Jun on 1/08/2017.
 */

public class ShapeHandler {

    //holds array of all shapes in the game
    private LinkedList<Shape> shapes_list;
    private Iterator<Shape> iter;
    public static ShapeHandler shapeHandler;
    private static Random rand = new Random();
    private int count = 0, speedCount = 0;
    public static float spawn_rate = 20;//distance between spawns
    private final int SPAWN_PERCENTAGE = 3;
    private Context context;
    private String TAG = ShapeHandler.class.getSimpleName();

    public LinkedList<PointsAnimation> pointsAnimations;

    private ShapeHandler(Context context){
        this.shapes_list = new LinkedList<>();
        this.context = context;
        this.pointsAnimations = new LinkedList<>();
    }

    public static void initShapeGenerator(Context context){
        shapeHandler = new ShapeHandler(context);
    }

    public synchronized void addShapeToList(Shape shape){
        shapes_list.add(shape);
    }

    public synchronized void removeShapeFromList(Shape shape){
        shapes_list.remove(shape);
    }

    public synchronized void addPointsAnimation(PointsAnimation pp){
        this.pointsAnimations.add(pp);
    }
    public synchronized void removePointsAnimation(PointsAnimation pp){
        this.pointsAnimations.remove(pp);
    }

    public void emptyList(){
        this.shapes_list.clear();
    }

    public void update(float delta){
        this.count++;

        if(count > spawn_rate){

            this.speedCount++;
            if(speedCount % 25 == 0) {
                if (SAMPO_MAIN.STARTING_SPEED < SAMPO_MAIN.TERMINAL_SPEED) {
                    SAMPO_MAIN.speed_increment += SAMPO_MAIN.SPEED_INCREMENT_AMOUNT;
                    //Log.w(TAG, "Speed incremented to : " + SAMPO_MAIN.speed_increment);
                    //inverse relationship between speed and distance.
                    ShapeHandler.spawn_rate = (1 / SAMPO_MAIN.speed_increment) * 7;
//                    Toast.makeText(getContext(),
//                            "Speed updated to: " + SAMPO_MAIN.speed_increment
//                            + "Spawn rate changed to : " + ShapeHandler.spawn_rate
//                            , Toast.LENGTH_SHORT).show();
                }
            }

            count = 0;
            if (rand.nextInt(10) > SPAWN_PERCENTAGE) {
                switch (rand.nextInt(12)) {//no){
                    case 0 : {
                        addShapeToList(new Square(context));
                        break;
                    }
                    case 1 : {
                        addShapeToList(new Square(context));
                        break;
                    }
                    case 2: {
                        addShapeToList(new Circle(context));
                        break;
                    }
                    case 3 : {
                        addShapeToList(new Circle(context));
                        break;
                    }
                    case 4: {
                        addShapeToList(new Triangle(context));
                        break;
                    }
                    case 5 : {
                        addShapeToList(new Triangle(context));
                        break;
                    }
                    case 6 : {
                        addShapeToList(new BadTriangle(context));
                        break;
                    }
                    case 7 : {
                        addShapeToList(new BadCircle(context));
                        break;
                    }
                    case 8 : {
                        addShapeToList(new BadSquare(context));
                        break;
                    }
                    case 9 : {
                        addShapeToList(new BadPentagon(context));
                        break;
                    }
                    case 10 : {
                        addShapeToList(new Pentagon(context));
                        break;
                    }
                    case 11 : {
                        addShapeToList(new Pentagon(context));
                        break;
                    }
                }
                //Log.i(TAG, "Shape spawned");
            }

        }

//        this.iter = shapes_list.iterator();
//        while(iter.hasNext()){
//            Shape shape = iter.next();
//            shape.update();
//            if(shape.b_remove){
//                removeShapeFromList(shape);
//                //shape = null;
//            }
//        }

        for (int i = shapes_list.size() - 1; i >= 0; i--){
            Shape shape = shapes_list.get(i);
            shape.update(delta);
            if(shape.b_remove){
                removeShapeFromList(shape);
                shape.bitmap.recycle();
                shape.bitmap = null;
                shape = null;
            }
        }

        for ( int j = this.pointsAnimations.size() - 1; j >= 0; j--){
            this.pointsAnimations.get(j).update();
        }

        //Iterate through, and update all shapes in the list.
    }

    public void draw(Canvas canvas, Paint paint, float delta){
        for(int i = 0; i < shapes_list.size(); i++){
            shapes_list.get(i).draw(canvas, paint, delta);
        }
        for(int j = 0; j < this.pointsAnimations.size(); j++){
            this.pointsAnimations.get(j).draw(canvas, paint);
        }
    }

    public LinkedList<Shape> getShapesList(){
        return this.shapes_list;
    }

}

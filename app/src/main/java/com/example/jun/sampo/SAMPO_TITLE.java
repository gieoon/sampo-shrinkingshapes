package com.example.jun.sampo;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

//TODO make the title have all sorts of different objects through it. Tiles of the four colours, red, green, blue, and yellow. And they are changing colour.
//show the Juuno logo here as loading, maximum 5 seconds, shoudl be about 3 seconds.
public class SAMPO_TITLE extends AppCompatActivity implements View.OnClickListener{

    private Button buttonPlay;
    private TextView scoreText;

    //make the blurred shapes scroll past.
    //make highscores visible, or one button from highscores.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int t_score = 0;
        //need to create a new bundle instead fo the instancestate
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if (bundle.getInt("score") != 0) {
                t_score = bundle.getInt("score");
            }
        }
        setContentView(R.layout.activity_sampo__title);

        //setting orientation to landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //getting the button
        buttonPlay = (Button) findViewById(R.id.buttonPlay);
        buttonPlay.setOnClickListener(this);

        scoreText = (TextView) findViewById(R.id.score);
        //if(SAMPO_MAIN.score == 0) SAMPO_MAIN.score = 1;
        scoreText.setText("" + ((t_score == 0) ? 0 : t_score));
        //SAMPO_MAIN.score = 0;

    }

    @Override
    public void onClick(View v){
        //starting game activity
        //SAMPO_MAIN.score = 1;
        SAMPO_MAIN.gamestate = SAMPO_MAIN.GAMESTATE.PLAYING;
        startActivity(new Intent(this, SAMPO_MAIN.class));
    }
}

package com.example.jun.sampo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static android.content.ContentValues.TAG;

/**
 * Created by Jun on 1/08/2017.
 */

public abstract class Shape {
    //BitMap to get character from image
    protected Bitmap bitmap, original_bitmap, blurred_bitmap;
    protected Context context;
    protected SizeManager sizeManager;
    protected PointsAnimation pointsAnimation;
    protected float speed, diagonal_movement;
    protected int scaled_width, scaled_height;
    protected float x, y;
    protected boolean b_movable, b_remove, b_updated_score, b_drawGlow, b_amIBad;
    protected boolean b_points_showing;
    protected Number number;
    //protected long startPointsDrawingTime;
    protected int worth;
    protected Glow glow;
    protected Blur blur;
    protected enum TYPE {
        GREEN,
        BLUE,
        YELLOW,
        RED,
        //added new values in here.
        //TRIANGLE,
        //CIRCLE,
        //SQUARE
    }
    protected TYPE type;
    protected enum SHAPE {
        TRIANGLE,
        CIRCLE,
        SQUARE,
        PENTAGON
        //NONE
    }

    private int badCount = 0;
    private final int BADCOUNTMAXVISIBLE = 80;
    private final int BADCOUNTMAXHIDDEN = 2;
    private boolean b_badDrawTrigger = false;

    private BlurMaskFilter bmf;

    protected SHAPE shapeType;

    protected static final Random rand = new Random();
    public static final List<TYPE> colorTypes = Collections.unmodifiableList(Arrays.asList(TYPE.values()));
    public static final int SIZE = colorTypes.size();
    public static final List<SHAPE> shapeTypes = Collections.unmodifiableList(Arrays.asList(SHAPE.values()));
    public static final int SHAPE_TYPE_SIZE = shapeTypes.size();

    protected float sizeDeduction;// = 1.80f;//number to divide all shapes by

    public Shape(Context context){
        this.context = context;
        this.b_updated_score = false;
        //get the startign speed and add a small random amount to it
        //10 different possible variations of speed.
        this.speed = SAMPO_MAIN.STARTING_SPEED + ((SAMPO_MAIN.STARTING_SPEED / 5.0f) * rand.nextInt(10));
        this.speed += SAMPO_MAIN.speed_increment;
        //Log.i("SHAPE","x: " + this.x);
        this.b_movable = false;//changed from false to true, can mvoe as soon as spawned
        this.b_remove = false;
        //this.number = new Number(this);
        this.b_drawGlow = false;
        this.sizeManager = new SizeManager(context, this);
        this.blur = new Blur(this.context);
        Random rand = new Random();
        //Getting bitmap from drawable resource
        //bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.shape);
        this.instantiateType();
        this.b_badDrawTrigger = true;
        this.bmf = new BlurMaskFilter(10, BlurMaskFilter.Blur.NORMAL);
        this.setSpawnSize();
        this.instantiateBitMap();
        this.setSpawnLocation();
        //this.blurred_bitmap = this.blur.blurImage(this.bitmap);
        this.b_amIBad = false;
        this.b_points_showing = false;
        this.diagonal_movement = (rand.nextFloat() * 10) - 5.0f;
        this.y = this.getHeight() * -1;//make it right above appearance
        //TODO temporarily removed
        //this.glow = new Glow(this.context, this);
    }

    protected void setSpawnSize(){
        this.sizeDeduction = (rand.nextFloat() * 3.5f + 1.0f);// * 8;
        //Log.w(TAG, "shape spawned with size" + this.sizeDeduction);
    }

    //sets location based on the size of the shape and adjusts where it spawns.
    protected void setSpawnLocation(){
        this.setX(rand.nextInt(TileGrid.GAME_WIDTH) - this.getWidth());
        //Log.w(TAG, "X is: " + this.getX() + "Game width is: " + TileGrid.GAME_WIDTH);
    }

//    protected boolean amIBad(){
//        if(rand.nextInt() % 2 == 0){
//            return true;
//        }
//        return false;
//    }

    protected abstract void instantiateBitMap();

    protected void instantiateType(){
        //this.type = TYPE.values()[rand.nextInt(TYPE.values().length)];
        //keeps it from caching it in memory all the time.
        this.type = colorTypes.get(rand.nextInt(SIZE));//minus the last three.
    }

    //bad shapes have a flashing, called with every update.
    protected void badFlash(){
        this.badCount++;
        if(this.b_badDrawTrigger) {
            if (this.badCount == BADCOUNTMAXVISIBLE) {
                this.badCount = 0;
                this.b_badDrawTrigger = false;
            }
        }
        else if(!this.b_badDrawTrigger){
            if(this.badCount == BADCOUNTMAXHIDDEN){
                this.b_badDrawTrigger = true;
                this.badCount = 0;
            }
        }
    }

    public void update(float delta){
        this.y += ((this.speed) * delta) + SAMPO_MAIN.speed_increment;
        //TEMPORARILY REMOVED!!
        //this.x += (this.diagonal_movement);

        if(SideShapesManager.currentShapeType == this.shapeType){ //|| BackgroundManager.currentType == this.type){
            this.sizeManager.update();
        }
        //updates twice as fast if both conditions are met.
        if(BackgroundManager.currentType == this.type){
            this.sizeManager.update();
        }

        if(this.y + ((this.getHeight() / 4.0f)  * 3.0f) > TileGrid.TOP_BOUNDARY){
            this.b_movable = true;
        }

        if(this.y + this.getHeight() > TileGrid.BOTTOM_BOUNDARY){
            if(!this.b_updated_score) {
                if(!this.b_amIBad) {
                    updateScore();
                    this.b_points_showing = true;
                    this.pointsAnimation = new PointsAnimation(this, this.context);
                    this.pointsAnimation.startDrawing();
                }
            }
            this.b_movable = false;
            this.b_updated_score = true;
        }
        //stops moving if bottom touches the line
        if(this.y + this.getHeight() - (this.getHeight() / 50) > TileGrid.BOTTOM_BOUNDARY){
            this.b_movable = false;
            if(this.b_amIBad){
                SAMPO_MAIN.b_gameover = true;
                SAMPO_MAIN.gamestate = SAMPO_MAIN.GAMESTATE.MENU;
                new Delay(60000, SAMPO_MAIN.GAMESTATE.MENU, this.context);
                MediaManager.loss_sound.start();
//                this.loss_sound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                    public void onCompletion(MediaPlayer mp) {
//                        mp.release();
//                    }
//                });
            }
        }
        if(this.y > TileGrid.REMOVAL_BOUNDARY){
            //delete and remove this shape.
            this.b_remove = true;
            //Log.w(TAG, "SHape set to remove");
        }

        //judge the boundaries from halfway of the shape.
        if(this.x + this.getWidth() / 2.0f < 0 || this.x - this.getWidth() / 2.0f > SAMPO_MAIN.WIDTH){
            this.b_movable = false;
            this.b_remove = true;
        }

        //check if points should be drawing.

        //automatically shoudl remove the text after shape is destroyed??
    }

    protected void updateScore(){
        //only update the combo if the score is positive!!!

        //SAMPO_MAIN.combo *= 2;

        //this.number.addToScore(this.getWidth());
        //otherwise restart the combo
        //if(this.number.getValue() < 0) {
          //  SAMPO_MAIN.combo = 1;
        //}
        this.worth = (int) (this.sizeManager.getSize() / 4.0f);
        SAMPO_MAIN.score += this.worth;
        SAMPO_MAIN.combo++;
        SAMPO_MAIN.score += SAMPO_MAIN.combo;

        MediaManager.playFirstAvailableSound(MediaManager.goal_sounds);
//        this.goal_sound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            public void onCompletion(MediaPlayer mp) {
//                mp.release();
//            }
//        });

    }


    public void draw(Canvas canvas, Paint paint, float delta){
        //Log.w(TAG, "Drawing X is: " + this.getX());
        paint.setColor(Color.TRANSPARENT);
        paint.setAlpha(150);
        if(SAMPO_MAIN.gamestate == SAMPO_MAIN.GAMESTATE.MENU){
            //blur it if it is the menu state
            paint.setMaskFilter(this.bmf);
        }
        if(!this.bitmap.isRecycled()) {
            //trigger is called for all shapes, both good & bad. Good shapes have it elft on true, bad shapes swap betweent he two.
            if(this.b_badDrawTrigger) {
                canvas.drawBitmap(this.imageChoice(), this.getX(), this.getY(), paint);
            }
        }
        //this.number.draw(canvas, paint);
        if(this.b_drawGlow){
            this.glow.draw(canvas, paint);
        }

        this.sizeManager.draw(canvas, paint);

    }

    protected Bitmap imageChoice(){
        //blurs the image if it is in menu, otherwise unblurs it.
        //Log.w(TAG, "gamestate is: " + SAMPO_MAIN.gamestate);
        //THIS IS CREATING ISSEUS
        //return (SAMPO_MAIN.gamestate == SAMPO_MAIN.GAMESTATE.MENU) ? this.blurred_bitmap : this.bitmap;
        //if(SAMPO_MAIN.gamestate == SAMPO_MAIN.GAMESTATE.PLAYING){
        //    return this.blurred_bitmap;
        //}
        //else{

            return this.bitmap;

        //}
    }

    protected void getResizedBitmap(Bitmap bitMap, int newWidth, int newHeight){
        int width = bitMap.getWidth();
        int height = bitMap.getHeight();
        this.scaled_width = newWidth;
        this.scaled_height = newHeight;
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        //create a matrix for the manipulation
        Matrix matrix = new Matrix();
        //resize the bitmap
        matrix.postScale(scaleWidth, scaleHeight);

        //recreate the new bitmap
//        if(Bitmap.createBitmap(bitMap, 0, 0, width, height, matrix, false).getWidth() > 0 ){
//
//        }
        //Log.w(TAG, (int)Math.log(width) + " : " +  (int)Math.log(height));
        //Bitmap resizedBitmap = Bitmap.createBitmap(bitMap, 0, 0,width, height, matrix, false);
        this.bitmap = Bitmap.createBitmap(bitMap, 0, 0,width, height, matrix, false);

        //if(bitMap != null && !bitMap.isRecycled()) {
            //bitMap.recycle();
            //bitMap = null;
        //}
        //return resizedBitmap;
    }

    public void adjustSize(float newSize){
        getResizedBitmap(this.original_bitmap, (int)newSize, (int)newSize);
    }


    //gets/sets
    public final int getWorth(){
        return this.worth;
    }
    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public final float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public final boolean getAlive(){
        return this.b_movable;
    }

    public final Shape.TYPE getType(){
        return this.type;
    }

    public final int getWidth(){
        return this.scaled_width;
    }
    public final int getHeight(){
        return this.scaled_height;
    }
    public void setGlowBoolean(boolean isGlowing){
        this.b_drawGlow = isGlowing;
    }
    public final boolean isGlowing(){
        return this.b_drawGlow;
    }

    public final Number getNumber(){
        return this.number;
    }
    public void setShowing(boolean b){
        this.b_points_showing = b;
    }
}

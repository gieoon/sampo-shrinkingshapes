package com.example.jun.sampo;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Jun on 12/08/2017.
 */

public class Square extends Shape {

    public Square(Context context){
        super(context);
        this.shapeType = SHAPE.SQUARE;
        //this.x -= this.scaled_width / 2;

    }

    @Override
    protected void instantiateBitMap(){
            switch(this.type){
                case GREEN : {
                    original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.square_g);
                    break;
                }
                case BLUE : {
                    original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.square_b);
                    break;
                }
                case YELLOW: {
                    original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.square_y);
                    break;
                }
                case RED : {
                    original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.square_r);
                    break;
                }
            }
            getResizedBitmap(this.original_bitmap, this.original_bitmap.getWidth() / (int) sizeDeduction, this.original_bitmap.getHeight() / (int) sizeDeduction);
            //this.sizeManager.setSize(this.bitmap.getWidth());
    }

    @Override
    public void update(float delta){
        super.update(delta);
    }

    @Override
    public void draw(Canvas canvas, Paint paint, float delta) {
        super.draw(canvas, paint, delta);
    }
}

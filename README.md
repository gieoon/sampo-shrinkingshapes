### SAMPO ShrinkingShapes

An Android game

# Actions
Scroll up and down to change the background color (yellow, green, blue, red)
Scroll sideways to change the shape (triangle, square, circle)

Shapes will fall from the top of the screen
- If the shape OR the background are the same as the ones falling down, the shape will shrink.
- The bigger the shape is when it reaches the bottom, the more points it is worth.
- As the game pgoresses, shapes will drop faster and faster.

Maximize your points!

Optional game mode - Some of the shapes are negative values/black and should not reach the bottom.
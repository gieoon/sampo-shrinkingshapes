package com.example.jun.sampo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Created by Jun on 7/09/2017.
 * Manages the size changes
 * Works closely with its shape;
 */

public class SizeManager{

    private Context context;
    private Shape shape;
    private float size;
    private final int COUNT_MAX = 2;
    private final float DECREASE_AMOUNT = 1.015f;//1.013
    private float decrease_amt_change = 0.000f;
    private float distance_from_starting_size = 0;
    private float SMALLEST_SIZE = 40;
    private int count = 0;

    public SizeManager(Context context, Shape shape){
        this.context = context;
        this.shape = shape;
        this.size = 0;
        //this.distance_from_starting_size = this.shape.getWidth() - this.SMALLEST_SIZE;
        //this.m.prepareAsync();
    }

    private void calculatePoints () {
        //if(this.shape.getNumber().getValue() > 0) {
            MediaManager.playFirstAvailableSound(MediaManager.loaded_sounds);
        //}
    }

    public void update() {
        if(this.shape.getAlive()) {

            this.count++;
            if (count == COUNT_MAX) {
                this.distance_from_starting_size = 1 / (this.shape.getWidth() - this.SMALLEST_SIZE);

                //Log.w(TAG, "adjusting Size to: " + this.size);
                this.size = this.shape.getWidth();
                float original_size = this.shape.getWidth();
                //if(this.size / DECREASE_AMOUNT > 0 && this.size > SMALLEST_SIZE ){
                if(this.size > SMALLEST_SIZE){
                    //the smaller it is, the more it decreses in size.
                    this.shape.adjustSize(this.size / (DECREASE_AMOUNT + this.distance_from_starting_size));
                    //adjust the shape to be back in the middle again.
                    //Log.w(TAG, "" + Math.log(this.shape.getWidth() / Math.log(1.0462)));
                    this.shape.setX(this.shape.getX() + ((original_size - this.shape.getWidth()) / 2));
                    //adjust the y as well
                    this.shape.setY(this.shape.getY() + ((original_size - this.shape.getWidth()) / 2));
                    this.count = 0;
                }
                else {
                    calculatePoints();
                    this.shape.b_remove = true;
                }
            }
        }
    }

    public void draw(Canvas canvas, Paint paint) {
        //NO ACTION
    }
    //called after the bitMap has been set ins shapes classes
    public void setSize(float size){
        this.size = size;
        //this.SMALLEST_SIZE = 100;
        //total size over a fixed number of decreases!!!
        //this.SMALLEST_SIZE = size / (DECREASE_AMOUNT * 50);
    }

    public float getSize(){
        return this.shape.getWidth();
    }
}

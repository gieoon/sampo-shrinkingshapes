package com.example.jun.sampo;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Jun on 20/09/2017.
 */

/**
 * Created by Jun on 12/08/2017.
 */

public class BadSquare extends Shape {

    public BadSquare(Context context){
        super(context);
        this.shapeType = SHAPE.SQUARE;
        this.b_amIBad = true;
        //this.x -= this.scaled_width / 2;

    }

    @Override
    protected void instantiateBitMap(){
        switch(this.type){
            case GREEN : {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bad_square_g);
                break;
            }
            case BLUE : {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bad_square_b);
                break;
            }
            case YELLOW: {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bad_square_y);
                break;
            }
            case RED : {
                this.original_bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bad_square_r);
                break;
            }
        }
        getResizedBitmap(this.original_bitmap, this.original_bitmap.getWidth() /(int) sizeDeduction, this.original_bitmap.getHeight() / (int)sizeDeduction);
        //this.sizeManager.setSize(this.bitmap.getWidth());
    }

    @Override
    public void update(float delta){
        super.update(delta);
        this.badFlash();
    }

    @Override
    public void draw(Canvas canvas, Paint paint, float delta) {
        super.draw(canvas, paint, delta);
    }
}

package com.example.jun.sampo;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

import java.util.Random;

import static android.content.ContentValues.TAG;

/**
 * Created by Jun on 21/08/2017.
 */

public class Number {
    private Random rand = new Random();
    private int value;
    private Shape shape;

    public Number(Shape shape){
        this.shape = shape;
        this.value = rand.nextInt(12) - 6; //random integer from -3 to 3
    }

    public void addToScore(float size){
        //TODO change value to be proportional to the size of the shape
        Log.w(TAG, "SIZE: " + size);
        //SAMPO_MAIN.score += this.value * SAMPO_MAIN.combo;
    }

    public void draw(Canvas canvas, Paint paint){
        paint.setColor(Color.GRAY);
        paint.setTextSize(32);
        float shapeMiddleX = this.shape.getX() + (this.shape.getWidth() / 2) - this.shape.getWidth() / 4;
        float shapeMiddleY = this.shape.getY() + (this.shape.getHeight() / 2) - this.shape.getWidth() / 4;
        if(this.value < 1){
            canvas.drawText(">O<", shapeMiddleX, shapeMiddleY, paint);
        }
        else {
            canvas.drawText("" + this.value, shapeMiddleX, shapeMiddleY, paint);
        }
    }

    public final int getValue(){
        return this.value;
    }
}

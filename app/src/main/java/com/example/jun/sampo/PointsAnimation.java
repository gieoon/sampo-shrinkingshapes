package com.example.jun.sampo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

/**
 * Created by Jun on 20/09/2017.
 */

public class PointsAnimation {

    private final Shape shape;
    private Context context;

    private final float MAX_DRAW_DURATION = 2.75f * 1000;
    private long startTime, endTime;
    private float y;
    private float alpha, middleX;
    private float textSize;
    private Typeface plain;

    public PointsAnimation(Shape shape, Context context){
        this.shape = shape;
        this.context = context;
        this.alpha = 200;
        this.middleX = this.shape.getX() + this.shape.getWidth() / 8;
        this.textSize = this.shape.getWidth() / 2.0f;
        this.plain = Typeface.createFromAsset(this.context.getAssets(), "Roboto-Black.ttf");
        this.y =  TileGrid.BOTTOM_BOUNDARY - TileGrid.MARGIN * 2;
        ShapeHandler.shapeHandler.addPointsAnimation(this);
    }

    public void startDrawing(){
        this.startTime = System.currentTimeMillis();
    }

    public void update() {
        this.alpha -= 0.2;
        this.textSize += 0.6;
        this.y -= 1.5f;
        //moves left as text gets larger
        this.middleX -= 0.1;
        //change opacity through updates here
        if(this.startTime != 0) {
            if (System.currentTimeMillis() - this.startTime > this.MAX_DRAW_DURATION) {
                ShapeHandler.shapeHandler.removePointsAnimation(this);
            }
        }

    }

    public void draw(Canvas canvas, Paint paint) {
        paint.setColor(Color.WHITE);
        paint.setTextSize(this.textSize);
        paint.setAlpha((int)this.alpha);
        paint.setTypeface(this.plain);
        canvas.drawText(Integer.toString( this.shape.getWorth()), this.middleX, this.y, paint);

        //reset defaults
        paint.setAlpha(100);
        paint.setTypeface(null);
    }

}
